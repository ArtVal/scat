name := "scat"

version := "1.0"

scalaVersion := "2.11.8"

val akkaV = "2.4.11"
val slickV = "3.1.1"
val slf4jNopV = "1.6.4"
val mysqlconV = "5.1.38"
val hickariV = "2.4.4"
val hsqldbV = "2.3.3"

mainClass in Compile := Some("WebServer")

libraryDependencies ++= Seq(
  "mysql" % "mysql-connector-java" % mysqlconV,
  "com.typesafe.slick" %% "slick" % slickV,
//  "org.slf4j" % "slf4j-nop" % slf4jNopV,
  //  "com.typesafe" % "config" % "1.3.0",
  //  "org.scalafx" %% "scalafx" % "8.0.60-R9",
  "com.typesafe.slick" % "slick-hikaricp_2.11" % slickV,
  "org.hsqldb" % "hsqldb" % hsqldbV,
  "com.typesafe.akka" %% "akka-actor" % akkaV,
  "com.typesafe.akka" %% "akka-http-experimental" % "2.4.11",
  "com.typesafe.akka" %% "akka-testkit" % akkaV,
  "com.typesafe.akka" %% "akka-http-spray-json-experimental" % "2.4.11",
//  "org.webjars" % "angularjs" % "1.5.8",
  "org.webjars" % "bootstrap" % "3.3.7-1",
  "org.webjars.bower" % "angular-ui-bootstrap-bower" % "2.2.0",
  "org.webjars.bower" % "angular-ui-select" % "0.19.4",
  "ch.qos.logback" % "logback-classic" % "1.1.7"
)

