/**
 * Created by artavorel on 29.08.16.
 */

angular.module('app').controller('MainController', ['$scope', 'DataService', function ($scope, DataService) {
    $scope.books = {
        page: {
            number: 1,
            size: 20,
            sort: 'id.asc',
            total: 1
        },
        wait: false,
        list: [],
        pageChanged: function () {
            var that = this;
            that.wait = true;
            DataService.getBooksPage(that.page.number,that.page.size,that.page.sort).then(function (response) {
                that.list = response.data.books;
                that.page.total = response.data.total;
            }).finally(function () {
                that.wait = false;
            });
        }
    };
    $scope.init = function () {
        $scope.books.pageChanged();
    };
    $scope.init();
}]);
