/**
 * Created by artavorel on 29.08.16.
 */
angular.module('app').service('DataService', ['$http', function ($http) {
    this.getAllBooks = function () {
        return $http.get('/api/books');
    }
    this.getBooksPage = function(page,size,sort) {
        return $http.get('/api/books',{
            params:{
                page:page,
                size:size,
                sort:sort
            }
        })
    }
}]);
