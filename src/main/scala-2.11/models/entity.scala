package models

/**
  * Created by artavorel on 27.10.16.
  */
object entity {

  case class Book(id: Int, title: String, place: Option[String], year: Option[Int])
  case class Books(books: Seq[Book])
  case class BookRecord(book: Book, pos: Option[Position], authors: Seq[Author], attributes: Seq[BookAttribute])
  case class BooksRecords(fullBooks: Seq[BookRecord])
  case class Position(id: Int, col: Int, row: Int, num: Option[Int])
  case class Author(id: Int, firstname: Option[String], lastname: Option[String], patronymic: Option[String])
  case class BookAttribute(id: Int, name: String, value: String, bookId: Int)
  case class BooksToAuthors(bookId: Int, authorId: Int)

  case class PageParams(page: Int, size: Int, sort: String)
  case class BookResource(book: Book,position: Option[Position],authors: Seq[Author],attributes: Seq[BookAttribute])
  case class BooksListPage(books: Seq[BookResource], total: Int)
}