import actors.BooksActor
import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.MediaTypes._
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.pattern._
import akka.stream.ActorMaterializer
import akka.util.Timeout
import models.entity._
import spray.json.DefaultJsonProtocol._
import spray.json._

import scala.concurrent.duration._
import scala.io.StdIn
import scala.util.{Failure, Success}

/**
  * Created by artavorel on 14.08.16.
  */
object WebServer {
  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem("scat")
    implicit val materializer = ActorMaterializer()
    // needed for the future flatMap/onComplete in the end
    implicit val executionContext = system.dispatcher
    implicit val timeout = Timeout(5000 seconds)
    implicit val bookFormat = jsonFormat4(Book)
    implicit val positionFormat = jsonFormat4(Position)
    implicit val authorFormat = jsonFormat4(Author)
    implicit val bookAttributeFormat = jsonFormat4(BookAttribute)
    implicit val fullBooksFormat = jsonFormat4(BookRecord)
    implicit val bookResourceFormat = jsonFormat4(BookResource)
    implicit val booksListPageFormat = jsonFormat2(BooksListPage)
    //    implicit val booksFormat = jsonFormat1(Books)
    val route = pathSingleSlash {
      getFromResource("webapp/index.html")
    } ~
      pathPrefix("webjars") {
        get {
          getFromResourceDirectory("META-INF/resources/webjars")
        }
      } ~
      path("api") {
        get {
          complete(StatusCodes.OK, "api")
        }
      } ~
      path("api" / "books") {
        get {
          parameters('page ? 0, 'size ? 30 ,'sort ? "id.asc") {
            (page,size,sort) => {
              onComplete(system.actorOf(Props[BooksActor]) ? PageParams(page, size, sort)){
                case Success(booksListPage: BooksListPage) =>
                  complete(
                    HttpResponse(
                      StatusCodes.OK,
                      entity = HttpEntity(`application/json`,booksListPage.toJson.toString())
                    )
                  )
                case Failure(ex) => complete(StatusCodes.InternalServerError, s"An error occurred: ${ex.getMessage}")
              }
            }
          }
        }
      } ~
      getFromResourceDirectory("webapp")

    val bindingFuture = Http().bindAndHandle(route, "localhost", 8080)

    println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done
  }
}

