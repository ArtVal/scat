val list = List(("a","s",1),("a","s",2),("a","s",3),("b","s",1))
list.groupBy{
  case(a,b,c) => (a,b)
} map {
  case(r,v) => (r._1,r._2,v.map(x => x._3))}
