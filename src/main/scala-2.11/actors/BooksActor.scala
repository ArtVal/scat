package actors

import akka.NotUsed
import akka.actor.Actor
import akka.pattern.pipe
import akka.stream.javadsl.ZipWith
import akka.stream.{ActorMaterializer, ClosedShape}
import akka.stream.scaladsl.{Broadcast, GraphDSL, RunnableGraph, Sink, Zip}
import models._
import models.entity._
import slick.driver.MySQLDriver
import slick.jdbc.JdbcBackend.Database
import storage._

import scala.Predef.augmentString
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


/**
  * Created by artavorel on 04.09.16.
  */
class BooksActor extends Actor {
  override def receive = {
    case ("list") => {
      val result = {
        BooksRepository.repository.findAll.map {
          b =>
            Future.sequence(b.map {
              book =>
                val res = for {
                  p <- PositionRepository.repository.findByBookId(book.id)
                  a <- BooksToAuthorsRepository
                    .repository
                    .findByBookId(book.id)
                    .map(r =>
                      Future.sequence(r.map(s =>
                        AuthorsRepository.repository.findById(s.authorId))))
                    .flatMap(r => r)
                  at <- AttributesRepository.repository.findByBookId(book.id)
                } yield (p, a, at)
                res.map(r => {
                  BookRecord(book, r._1, r._2.flatten, r._3)
                })
            }).map(r => {
              BooksRecords(r)
            })
        }
      }
      result.flatMap(r => r).pipeTo(sender)
    }
    case (params: PageParams) => {
      implicit val materializer = ActorMaterializer()
      BooksRepository.repository.booksResourcesList(params)
        .runWith(Sink.head)
        .map(r => r) pipeTo sender
    }  
  }
}
