package storage

import models.entity.Position

/**
  * Created by artavorel on 23.10.16.
  */
trait PositionModule {
  self: DatabaseProfile =>

  import profile.api._

//  case class Position(id: Int, col: Int, row: Int, num: Option[Int])

  class PositionTable(tag: Tag) extends Table[Position](tag, "position") {
    def id = column[Int]("book_id", O.PrimaryKey, O.AutoInc)
    def col = column[Int]("col")
    def row = column[Int]("row")
    def num = column[Option[Int]]("cell_position")
    def * = (id, col, row, num) <> (Position.tupled, Position.unapply)
  }

  lazy val positions = TableQuery[PositionTable]

}
