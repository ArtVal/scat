package storage

import slick.driver.{JdbcProfile, MySQLDriver}

/**
  * Created by artavorel on 23.10.16.
  */
trait DatabaseProfile {
  val profile: JdbcProfile = MySQLDriver
  import profile.api._
  val db = Database.forConfig("mysqldb")
}
