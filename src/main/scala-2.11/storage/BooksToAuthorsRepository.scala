package storage

/**
  * Created by artavorel on 25.10.16.
  */
class BooksToAuthorsRepository extends BooksToAuthorsModule with DatabaseProfile {

  import  profile.api._

  def findAll = {
    db.run(booksToAuthors.result)
  }

  def findByAuthorId(id: Int) = {
    db.run(booksToAuthors.filter(_.authorId === id).result)
  }

  def findByBookId(id: Int) = {
    db.run(booksToAuthors.filter(_.bookId === id).result)
  }

}

object BooksToAuthorsRepository {
  val repository = new BooksToAuthorsRepository
}
