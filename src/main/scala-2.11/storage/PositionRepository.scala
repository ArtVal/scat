package storage

import akka.NotUsed
import akka.stream.scaladsl.Flow
import models.entity.Position

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by artavorel on 25.10.16.
  */
class PositionRepository extends PositionModule with DatabaseProfile {

  import profile.api._

  def findAll = {
    db.run(positions.result)
  }

  def findByBookId(id: Int) = {
    db.run(positions.filter(_.id === id).result.headOption)
  }


}

object PositionRepository {
  val repository = new PositionRepository
}
