package storage


import akka.stream._
import akka.stream.scaladsl._
import akka.NotUsed
import models.entity._
import slick.jdbc.{ActionBasedSQLInterpolation, GetResult, SQLActionBuilder}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.math.Ordering.Implicits._
import scala.concurrent.Future
import scala.util.Success

/**
  * Created by artavorel on 23.10.16.
  */
class BooksRepository extends BooksModule
  with PositionModule
  with BooksToAuthorsModule
  with AuthorsModule
  with AttributesModule
  with DatabaseProfile {

  import profile.api._

  def findAll = {
    db.run(books.result)
  }

  def findById(id: Int) = {
    db.run(books.filter(_.id === id).result.headOption)
  }

  def findBooksResourcesIdsQuery(params: PageParams): Source[Int, NotUsed] = {
    val skip = params.page match {
      case v if v <= 0 => 0
      case v => (v - 1) * params.size
    }
    val take = params.size

    val order = params.sort match {
      case "id.desc" => "b.id DESC"
      case "title.asc" => "b.title ASC"
      case "title.desc" => "b.title DESC"
      case "year.asc" => "b.publication_year ASC"
      case "year.desc" => "b.publication_year DESC"
      case "place.asc" => "b.publication_place ASC"
      case "place.desc" => "b.publication_place DESC"
      case _ => "b.id ASC"
    }
    val queryBodyPart = queryBody

    val query =
      sql"""
            SELECT b.id
            #$queryBodyPart
            GROUP BY b.id
            ORDER BY #$order
            LIMIT $take
            OFFSET $skip
         """.as[Int]

    Source.fromPublisher {
      db.stream {
        query
          .transactionally
          .withStatementParameters(fetchSize = 1000)
      }
    }
  }

  def booksTotal(params: PageParams) = {
    val queryBodyPart = queryBody
    val query =
      sql"""
            SELECT DISTINCT COUNT(b.id)
            #$queryBodyPart

         """.as[Int]
    db.run(query)
  }

  def queryBody = {
    s"""
       | FROM books b
       | LEFT JOIN position p ON b.id = p.book_id
       | LEFT JOIN booksauthors bauth ON bauth.book_id = b.id
       | LEFT JOIN authors a ON a.id = bauth.author_id
       | LEFT JOIN booksattributes attr ON attr.book_id = b.id
     """.stripMargin
  }

  def booksResourcesList(params: PageParams) = {
    findBooksResourcesIdsQuery(params)
      .mapAsync(1)(
        id => BooksRepository.repository.findById(id)
          .collect {
            case Some(b) => b
          }
          .map {
            book => (id, book)
          }
      )
      .mapAsync(1)(r => {
        PositionRepository.repository.findByBookId(r._1).map {
          pos => (r._1, r._2, pos)
        }
      })
      .mapAsync(1)(
        r => BooksToAuthorsRepository.repository.findByBookId(r._1).map {
          aIds => (r._1, r._2, r._3, aIds)
        }
      )
      .mapAsync(1)(
        r => {
          Future.sequence(
            r._4.map(
              bta => AuthorsRepository.repository.findById(bta.authorId)
                .collect {
                  case Some(a) => a
                }
            )
          )
            .map(authors => (r._1, r._2, r._3, authors))
        }
      )
      .mapAsync(1)(
        r => {
          AttributesRepository.repository.findByBookId(r._1).map {
            attr => BookResource(r._2, r._3, r._4, attr)
          }
        }
      )
      .fold(Seq[BookResource]())((s, b) => s :+ b)
      .mapAsync(1)(books => {
        booksTotal(params).map(total => BooksListPage(books,total.head))
      })
  }


}

object BooksRepository {
  val repository = new BooksRepository
}