package storage

import models.entity.BookAttribute

/**
  * Created by artavorel on 23.10.16.
  */
trait AttributesModule {
  self: DatabaseProfile =>

  import profile.api._

//  case class BookAttribute(id: Int, name: String, value: String, bookId: Int)

  class BookAttributeTable(tag: Tag) extends Table[BookAttribute](tag, "booksattributes") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def value = column[String]("value")
    def bookId = column[Int]("book_id")
    def * = (id, name, value, bookId) <> (BookAttribute.tupled, BookAttribute.unapply)
  }

  lazy val bookAttributes = TableQuery[BookAttributeTable]


}
