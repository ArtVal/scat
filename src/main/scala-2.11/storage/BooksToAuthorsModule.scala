package storage

import models.entity.BooksToAuthors

/**
  * Created by artavorel on 23.10.16.
  */
trait BooksToAuthorsModule {
  self: DatabaseProfile =>

  import profile.api._

//  case class BooksToAuthors(bookId: Int, authorId: Int)

  class BooksToAuthorsTable(tag: Tag) extends Table[BooksToAuthors](tag, "booksauthors") {
    def bookId = column[Int]("book_id")
    def authorId = column[Int]("author_id")
    def * = (bookId, authorId) <> (BooksToAuthors.tupled, BooksToAuthors.unapply)
  }

  lazy val booksToAuthors = TableQuery[BooksToAuthorsTable]

}
