package storage

import models.entity.Author


/**
  * Created by artavorel on 23.10.16.
  */
trait AuthorsModule {
  self: DatabaseProfile =>

  import profile.api._

//  case class Author(id: Int,
//                    firstname: Option[String],
//                    lastname: Option[String],
//                    patronymic: Option[String])

  class AuthorsTable(tag: Tag) extends Table[Author](tag, "authors") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def firstname = column[Option[String]]("first_name")
    def lastname = column[Option[String]]("last_name")
    def patronymic = column[Option[String]]("patronymic")
    def * = (id, firstname, lastname, patronymic) <> (Author.tupled, Author.unapply)
  }

  lazy val authors = TableQuery[AuthorsTable]

}
