package storage

/**
  * Created by artavorel on 25.10.16.
  */
class AuthorsRepository extends AuthorsModule with DatabaseProfile {

  import profile.api._

  def findAll = {
    db.run(authors.result)
  }

  def findById(id: Int) = {
    db.run(authors.filter(_.id === id).result.headOption)
  }

}

object AuthorsRepository {
  val repository = new AuthorsRepository
}
