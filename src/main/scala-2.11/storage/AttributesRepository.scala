package storage

/**
  * Created by artavorel on 25.10.16.
  */
class AttributesRepository extends AttributesModule with DatabaseProfile {

  import profile.api._

  def findAll = {
    db.run(bookAttributes.result)
  }

  def findByBookId(bookId: Int) = {
    db.run(bookAttributes.filter(_.bookId === bookId).result)
  }

  def findByBookIdAndName(bookId: Int, name: String) = {
    db.run(bookAttributes.filter{ attr => attr.bookId === bookId && attr.name === name}.result)
  }
}

object AttributesRepository {
  val repository = new AttributesRepository
}
