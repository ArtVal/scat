package storage

import models.entity.Book

/**
  * Created by artavorel on 23.10.16.
  */
trait BooksModule {
  self: DatabaseProfile =>

  import profile.api._

//  case class Book(id: Int, title: String, place: Option[String], year: Option[Int])

  class BooksTable(tag: Tag) extends Table[Book](tag, "books") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def title = column[String]("title")
    def place = column[Option[String]]("publication_place")
    def year = column[Option[Int]]("publication_year")
    def * = (id, title, place, year) <> (Book.tupled, Book.unapply)
  }

  lazy val books = TableQuery[BooksTable]


}
